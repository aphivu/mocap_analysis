# Préparation de l'environnement de travail

## Installation du package **pyomeca**

La réalisation du TP implique l'utilisation du package python [pyomeca](https://pyomeca.github.io/getting-started/). 
Dans l'environnement d'Anaconda, l'installation du package et de ses dépendances peut se faire via cette ligne de commande. L'installation peut prendre plusieurs minutes.

```
conda install -c conda-forge pyomeca
```

## Préparation du projet 

Dans **spyder**, créez un nouveau projet dans lequel vous devrez copier-coller vos fichiers **c3d** à traiter dans un dossier nommé **data**.
Créez un nouveau script python intitulé **mocap_data_process.py**.

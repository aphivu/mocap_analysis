# Introduction - TP MOCAP 1A ENS

Ce petit guide a pour vocation d'introduire l'analyse de données avec python dans le cadre du TP de MOCAP du département 2SEP en 1A. 
Dans les grandes lignes, seront abordées les sections suivantes :

```{tableofcontents}
```


:::{note}
Cette partie du TP ne s'applique qu'après avoir correctement labelisé et exporté ses captures au format **c3d**.
:::

